## Test weld
```gcode
G91		; Set relative movement
M92 E106.01	; Set the number of steps per mm for the extruder
M106 P1		; Turn on exhaust fan
G1 X2		; Start move
M106 P0		; Turn on welder
G1 E60 F300 X20	; Extrude welding wire 60mm along 20mm move
M107 P0		; Turn off welder
G1 X2		; Finish off rest of the move
;M107 P1	; Turn off exhaust fan
G92 X0 Y0 Z0
```

## Take measurement routine

### When check box is checked
```gcode
G92 X0 Y0 Z0
G91		; Set relative movement
G1 Z20		; Move up 20mm to get out of the way of the bolts
G1 X50 Y170 F3000	; Push bed out and move X over
G91		; Set relative movement
```

### When unchecked
```gcode
G90	; Set absolute movement
G1 X0 Y0 Z20 F3000
G1 Z0	; Move back to beginging of substrate and bring Z back down to regular level.
G91
```

## Move to next testing batch button
```gcode
G91
G1 Y4
G1 X-120
G92 X0 Y0 Z0
```

## Move to beginning starting position
```gcode
; Has to be after homing X, Y, then Z
G90
G1 Y47 Z10
G1 X47 Z5
G92 X0 Y0 Z0
```

## Numbers to remember for tests
- Extruder is 106.01 steps per mm.
`~24 Amps` - For welder current.
- Needs to be in TIG mode.
- Gas need to be on.
- Relay wires need to be connected.
- Grounding wire need to be in secure contact with both the substrate and the non-stick welding port on the welder.
- Substrate is `6.41mm - 6.61mm` thick.
- Home all axis'
- Z +5mm
- X +37mm
- Y +37mm

## Set steps per mm of an axis
`M92 E100` - 

## Emergency stop
`M112` - Not instantaneous on our machine...

## Set zero point for stepper
`G92 E0` - Will set the current point to "0" for the "E" axis.

## Linear move
`G1 E10` - Will move to 10mm on the extruder axis.

## Turn on fan/welder
`M106 P0 S255` - Turn on port 0's fan to full PWM speed.

## Turn off fan/welder
`M107 P0` - Turn off port 0's fan/set it to PWM 0.

## Firmware info
`M115` - Version info and some other jazz.

## Home an axis
`G28 X Y Z` - Home the X, Y, and Z axis. 
