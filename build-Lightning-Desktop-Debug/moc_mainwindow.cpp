/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.8)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Lightning/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.8. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[35];
    char stringdata0[685];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 21), // "on_pbConnect_released"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 14), // "UpdateTerminal"
QT_MOC_LITERAL(4, 49, 7), // "Message"
QT_MOC_LITERAL(5, 57, 15), // "Fill_Baud_Rates"
QT_MOC_LITERAL(6, 73, 18), // "on_pbSend_released"
QT_MOC_LITERAL(7, 92, 26), // "on_lnCommand_returnPressed"
QT_MOC_LITERAL(8, 119, 25), // "ConnectionSettingsChanged"
QT_MOC_LITERAL(9, 145, 4), // "Port"
QT_MOC_LITERAL(10, 150, 4), // "Baud"
QT_MOC_LITERAL(11, 155, 24), // "on_pushButton_3_released"
QT_MOC_LITERAL(12, 180, 24), // "on_pushButton_7_released"
QT_MOC_LITERAL(13, 205, 24), // "on_pushButton_6_released"
QT_MOC_LITERAL(14, 230, 24), // "on_pushButton_5_released"
QT_MOC_LITERAL(15, 255, 24), // "on_pushButton_4_released"
QT_MOC_LITERAL(16, 280, 24), // "on_pushButton_2_released"
QT_MOC_LITERAL(17, 305, 16), // "on_xDec_released"
QT_MOC_LITERAL(18, 322, 16), // "on_xInc_released"
QT_MOC_LITERAL(19, 339, 16), // "on_yDec_released"
QT_MOC_LITERAL(20, 356, 16), // "on_yInc_released"
QT_MOC_LITERAL(21, 373, 16), // "on_zDec_released"
QT_MOC_LITERAL(22, 390, 16), // "on_zInc_released"
QT_MOC_LITERAL(23, 407, 27), // "on_endEffectToggler_toggled"
QT_MOC_LITERAL(24, 435, 7), // "checked"
QT_MOC_LITERAL(25, 443, 29), // "on_incrementSize_valueChanged"
QT_MOC_LITERAL(26, 473, 4), // "arg1"
QT_MOC_LITERAL(27, 478, 21), // "on_exhaustFan_toggled"
QT_MOC_LITERAL(28, 500, 24), // "on_testWeldXInc_released"
QT_MOC_LITERAL(29, 525, 22), // "on_pushButton_released"
QT_MOC_LITERAL(30, 548, 27), // "on_moveToNextXTest_released"
QT_MOC_LITERAL(31, 576, 27), // "on_moveToNextZTest_released"
QT_MOC_LITERAL(32, 604, 29), // "on_measurementRoutine_toggled"
QT_MOC_LITERAL(33, 634, 27), // "on_startupPosition_released"
QT_MOC_LITERAL(34, 662, 22) // "on_setZHeight_released"

    },
    "MainWindow\0on_pbConnect_released\0\0"
    "UpdateTerminal\0Message\0Fill_Baud_Rates\0"
    "on_pbSend_released\0on_lnCommand_returnPressed\0"
    "ConnectionSettingsChanged\0Port\0Baud\0"
    "on_pushButton_3_released\0"
    "on_pushButton_7_released\0"
    "on_pushButton_6_released\0"
    "on_pushButton_5_released\0"
    "on_pushButton_4_released\0"
    "on_pushButton_2_released\0on_xDec_released\0"
    "on_xInc_released\0on_yDec_released\0"
    "on_yInc_released\0on_zDec_released\0"
    "on_zInc_released\0on_endEffectToggler_toggled\0"
    "checked\0on_incrementSize_valueChanged\0"
    "arg1\0on_exhaustFan_toggled\0"
    "on_testWeldXInc_released\0"
    "on_pushButton_released\0"
    "on_moveToNextXTest_released\0"
    "on_moveToNextZTest_released\0"
    "on_measurementRoutine_toggled\0"
    "on_startupPosition_released\0"
    "on_setZHeight_released"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      28,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  154,    2, 0x08 /* Private */,
       3,    1,  155,    2, 0x08 /* Private */,
       5,    0,  158,    2, 0x08 /* Private */,
       6,    0,  159,    2, 0x08 /* Private */,
       7,    0,  160,    2, 0x08 /* Private */,
       8,    2,  161,    2, 0x08 /* Private */,
      11,    0,  166,    2, 0x08 /* Private */,
      12,    0,  167,    2, 0x08 /* Private */,
      13,    0,  168,    2, 0x08 /* Private */,
      14,    0,  169,    2, 0x08 /* Private */,
      15,    0,  170,    2, 0x08 /* Private */,
      16,    0,  171,    2, 0x08 /* Private */,
      17,    0,  172,    2, 0x08 /* Private */,
      18,    0,  173,    2, 0x08 /* Private */,
      19,    0,  174,    2, 0x08 /* Private */,
      20,    0,  175,    2, 0x08 /* Private */,
      21,    0,  176,    2, 0x08 /* Private */,
      22,    0,  177,    2, 0x08 /* Private */,
      23,    1,  178,    2, 0x08 /* Private */,
      25,    1,  181,    2, 0x08 /* Private */,
      27,    1,  184,    2, 0x08 /* Private */,
      28,    0,  187,    2, 0x08 /* Private */,
      29,    0,  188,    2, 0x08 /* Private */,
      30,    0,  189,    2, 0x08 /* Private */,
      31,    0,  190,    2, 0x08 /* Private */,
      32,    1,  191,    2, 0x08 /* Private */,
      33,    0,  194,    2, 0x08 /* Private */,
      34,    0,  195,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    9,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   24,
    QMetaType::Void, QMetaType::Double,   26,
    QMetaType::Void, QMetaType::Bool,   24,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   24,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_pbConnect_released(); break;
        case 1: _t->UpdateTerminal((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->Fill_Baud_Rates(); break;
        case 3: _t->on_pbSend_released(); break;
        case 4: _t->on_lnCommand_returnPressed(); break;
        case 5: _t->ConnectionSettingsChanged((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< qint32(*)>(_a[2]))); break;
        case 6: _t->on_pushButton_3_released(); break;
        case 7: _t->on_pushButton_7_released(); break;
        case 8: _t->on_pushButton_6_released(); break;
        case 9: _t->on_pushButton_5_released(); break;
        case 10: _t->on_pushButton_4_released(); break;
        case 11: _t->on_pushButton_2_released(); break;
        case 12: _t->on_xDec_released(); break;
        case 13: _t->on_xInc_released(); break;
        case 14: _t->on_yDec_released(); break;
        case 15: _t->on_yInc_released(); break;
        case 16: _t->on_zDec_released(); break;
        case 17: _t->on_zInc_released(); break;
        case 18: _t->on_endEffectToggler_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->on_incrementSize_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 20: _t->on_exhaustFan_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: _t->on_testWeldXInc_released(); break;
        case 22: _t->on_pushButton_released(); break;
        case 23: _t->on_moveToNextXTest_released(); break;
        case 24: _t->on_moveToNextZTest_released(); break;
        case 25: _t->on_measurementRoutine_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 26: _t->on_startupPosition_released(); break;
        case 27: _t->on_setZHeight_released(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 28)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 28;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 28)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 28;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
