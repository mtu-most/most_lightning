/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSelect_a_rate;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_5;
    QCheckBox *exhaustFan;
    QCheckBox *measurementRoutine;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QCheckBox *endEffectToggler;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_6;
    QPushButton *pushButton_5;
    QPushButton *pushButton_4;
    QLineEdit *lnCommand;
    QTextEdit *txtTerminal;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QComboBox *comboBox_2;
    QComboBox *comboBox;
    QPushButton *pbConnect;
    QGroupBox *groupBox_2;
    QLabel *label;
    QDoubleSpinBox *incrementSize;
    QPushButton *testWeldXInc;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_3;
    QPushButton *zDec;
    QPushButton *xDec;
    QPushButton *yDec;
    QPushButton *xInc;
    QPushButton *yInc;
    QPushButton *zInc;
    QPushButton *moveToNextTest;
    QPushButton *startupPosition;
    QPushButton *pbSend;
    QPushButton *pushButton_7;
    QGroupBox *gbx_Test_Parameters;
    QGridLayout *gridLayout_4;
    QLabel *label_2;
    QLabel *label_3;
    QLineEdit *lnExtrusion;
    QLineEdit *lnFeedrate;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuConnections;
    QMenu *menuBaud_Rate;
    QMenu *menuPSO_Portal;
    QMenu *menuDAQ_Portal;
    QMenu *menuSettings;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(927, 653);
        actionSelect_a_rate = new QAction(MainWindow);
        actionSelect_a_rate->setObjectName(QString::fromUtf8("actionSelect_a_rate"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        verticalLayout_2 = new QVBoxLayout(groupBox_3);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        exhaustFan = new QCheckBox(groupBox_3);
        exhaustFan->setObjectName(QString::fromUtf8("exhaustFan"));

        horizontalLayout_5->addWidget(exhaustFan);

        measurementRoutine = new QCheckBox(groupBox_3);
        measurementRoutine->setObjectName(QString::fromUtf8("measurementRoutine"));

        horizontalLayout_5->addWidget(measurementRoutine);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        pushButton = new QPushButton(groupBox_3);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_2->addWidget(pushButton);

        pushButton_2 = new QPushButton(groupBox_3);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout_2->addWidget(pushButton_2);

        endEffectToggler = new QCheckBox(groupBox_3);
        endEffectToggler->setObjectName(QString::fromUtf8("endEffectToggler"));

        horizontalLayout_2->addWidget(endEffectToggler);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton_6 = new QPushButton(groupBox_3);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));

        horizontalLayout->addWidget(pushButton_6);

        pushButton_5 = new QPushButton(groupBox_3);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        horizontalLayout->addWidget(pushButton_5);

        pushButton_4 = new QPushButton(groupBox_3);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        horizontalLayout->addWidget(pushButton_4);


        verticalLayout_2->addLayout(horizontalLayout);


        gridLayout->addWidget(groupBox_3, 8, 3, 2, 1);

        lnCommand = new QLineEdit(centralWidget);
        lnCommand->setObjectName(QString::fromUtf8("lnCommand"));

        gridLayout->addWidget(lnCommand, 11, 1, 1, 1);

        txtTerminal = new QTextEdit(centralWidget);
        txtTerminal->setObjectName(QString::fromUtf8("txtTerminal"));
        txtTerminal->setMaximumSize(QSize(450, 16777215));

        gridLayout->addWidget(txtTerminal, 0, 1, 10, 2);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        comboBox_2 = new QComboBox(groupBox);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));

        gridLayout_2->addWidget(comboBox_2, 1, 0, 1, 1);

        comboBox = new QComboBox(groupBox);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout_2->addWidget(comboBox, 1, 1, 1, 1);

        pbConnect = new QPushButton(groupBox);
        pbConnect->setObjectName(QString::fromUtf8("pbConnect"));

        gridLayout_2->addWidget(pbConnect, 0, 0, 1, 2);


        gridLayout->addWidget(groupBox, 0, 3, 2, 1);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy);
        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(80, 20, 141, 20));
        incrementSize = new QDoubleSpinBox(groupBox_2);
        incrementSize->setObjectName(QString::fromUtf8("incrementSize"));
        incrementSize->setGeometry(QRect(230, 20, 71, 26));
        incrementSize->setValue(1.000000000000000);
        testWeldXInc = new QPushButton(groupBox_2);
        testWeldXInc->setObjectName(QString::fromUtf8("testWeldXInc"));
        testWeldXInc->setGeometry(QRect(10, 50, 121, 28));
        layoutWidget = new QWidget(groupBox_2);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(70, 80, 337, 131));
        gridLayout_3 = new QGridLayout(layoutWidget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setSizeConstraint(QLayout::SetMaximumSize);
        gridLayout_3->setHorizontalSpacing(5);
        gridLayout_3->setVerticalSpacing(3);
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        zDec = new QPushButton(layoutWidget);
        zDec->setObjectName(QString::fromUtf8("zDec"));

        gridLayout_3->addWidget(zDec, 2, 3, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        xDec = new QPushButton(layoutWidget);
        xDec->setObjectName(QString::fromUtf8("xDec"));
        xDec->setMaximumSize(QSize(85, 28));

        gridLayout_3->addWidget(xDec, 1, 0, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        yDec = new QPushButton(layoutWidget);
        yDec->setObjectName(QString::fromUtf8("yDec"));

        gridLayout_3->addWidget(yDec, 2, 1, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        xInc = new QPushButton(layoutWidget);
        xInc->setObjectName(QString::fromUtf8("xInc"));

        gridLayout_3->addWidget(xInc, 1, 2, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        yInc = new QPushButton(layoutWidget);
        yInc->setObjectName(QString::fromUtf8("yInc"));

        gridLayout_3->addWidget(yInc, 0, 1, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        zInc = new QPushButton(layoutWidget);
        zInc->setObjectName(QString::fromUtf8("zInc"));

        gridLayout_3->addWidget(zInc, 0, 3, 1, 1);

        moveToNextTest = new QPushButton(groupBox_2);
        moveToNextTest->setObjectName(QString::fromUtf8("moveToNextTest"));
        moveToNextTest->setGeometry(QRect(140, 50, 121, 28));
        startupPosition = new QPushButton(groupBox_2);
        startupPosition->setObjectName(QString::fromUtf8("startupPosition"));
        startupPosition->setGeometry(QRect(0, 180, 61, 28));

        gridLayout->addWidget(groupBox_2, 2, 3, 5, 1);

        pbSend = new QPushButton(centralWidget);
        pbSend->setObjectName(QString::fromUtf8("pbSend"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pbSend->sizePolicy().hasHeightForWidth());
        pbSend->setSizePolicy(sizePolicy1);
        pbSend->setMaximumSize(QSize(80, 300));
        pbSend->setBaseSize(QSize(60, 22));

        gridLayout->addWidget(pbSend, 11, 2, 1, 1);

        pushButton_7 = new QPushButton(centralWidget);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(204, 0, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(255, 51, 51, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(229, 25, 25, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(102, 0, 0, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(136, 0, 0, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush6(QColor(255, 255, 255, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        QBrush brush7(QColor(229, 127, 127, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush7);
        QBrush brush8(QColor(255, 255, 220, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        QBrush brush9(QColor(0, 0, 0, 128));
        brush9.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Active, QPalette::PlaceholderText, brush9);
#endif
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        QBrush brush10(QColor(0, 0, 0, 128));
        brush10.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush10);
#endif
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        QBrush brush11(QColor(0, 0, 0, 128));
        brush11.setStyle(Qt::NoBrush);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush11);
#endif
        pushButton_7->setPalette(palette);
        pushButton_7->setFlat(false);

        gridLayout->addWidget(pushButton_7, 11, 3, 1, 1);

        gbx_Test_Parameters = new QGroupBox(centralWidget);
        gbx_Test_Parameters->setObjectName(QString::fromUtf8("gbx_Test_Parameters"));
        gridLayout_4 = new QGridLayout(gbx_Test_Parameters);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        label_2 = new QLabel(gbx_Test_Parameters);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_4->addWidget(label_2, 0, 0, 1, 1);

        label_3 = new QLabel(gbx_Test_Parameters);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_4->addWidget(label_3, 1, 0, 1, 1);

        lnExtrusion = new QLineEdit(gbx_Test_Parameters);
        lnExtrusion->setObjectName(QString::fromUtf8("lnExtrusion"));

        gridLayout_4->addWidget(lnExtrusion, 1, 1, 1, 1);

        lnFeedrate = new QLineEdit(gbx_Test_Parameters);
        lnFeedrate->setObjectName(QString::fromUtf8("lnFeedrate"));

        gridLayout_4->addWidget(lnFeedrate, 0, 1, 1, 1);


        gridLayout->addWidget(gbx_Test_Parameters, 7, 3, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 927, 19));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuConnections = new QMenu(menuBar);
        menuConnections->setObjectName(QString::fromUtf8("menuConnections"));
        menuBaud_Rate = new QMenu(menuConnections);
        menuBaud_Rate->setObjectName(QString::fromUtf8("menuBaud_Rate"));
        menuPSO_Portal = new QMenu(menuBar);
        menuPSO_Portal->setObjectName(QString::fromUtf8("menuPSO_Portal"));
        menuDAQ_Portal = new QMenu(menuBar);
        menuDAQ_Portal->setObjectName(QString::fromUtf8("menuDAQ_Portal"));
        menuSettings = new QMenu(menuBar);
        menuSettings->setObjectName(QString::fromUtf8("menuSettings"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuConnections->menuAction());
        menuBar->addAction(menuPSO_Portal->menuAction());
        menuBar->addAction(menuDAQ_Portal->menuAction());
        menuBar->addAction(menuSettings->menuAction());
        menuConnections->addAction(menuBaud_Rate->menuAction());
        menuConnections->addSeparator();
        menuBaud_Rate->addAction(actionSelect_a_rate);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        actionSelect_a_rate->setText(QApplication::translate("MainWindow", "Select a rate", nullptr));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Functions", nullptr));
        exhaustFan->setText(QApplication::translate("MainWindow", "Exaust Fan", nullptr));
        measurementRoutine->setText(QApplication::translate("MainWindow", "Measurement Routine", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "Run G-Code", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "Pause", nullptr));
        endEffectToggler->setText(QApplication::translate("MainWindow", "End Effect", nullptr));
        pushButton_6->setText(QApplication::translate("MainWindow", "Home X", nullptr));
        pushButton_5->setText(QApplication::translate("MainWindow", "Home Y", nullptr));
        pushButton_4->setText(QApplication::translate("MainWindow", "Home Z", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "Connection", nullptr));
        pbConnect->setText(QApplication::translate("MainWindow", "Connect", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Positioning", nullptr));
        label->setText(QApplication::translate("MainWindow", "Increment Size (in mm)", nullptr));
        testWeldXInc->setText(QApplication::translate("MainWindow", "Test Weld in X+", nullptr));
        zDec->setText(QApplication::translate("MainWindow", "Z-", nullptr));
        xDec->setText(QApplication::translate("MainWindow", "X-", nullptr));
        yDec->setText(QApplication::translate("MainWindow", "Y-", nullptr));
        xInc->setText(QApplication::translate("MainWindow", "X+", nullptr));
        yInc->setText(QApplication::translate("MainWindow", "Y+", nullptr));
        zInc->setText(QApplication::translate("MainWindow", "Z+", nullptr));
        moveToNextTest->setText(QApplication::translate("MainWindow", "Move to next test", nullptr));
        startupPosition->setText(QApplication::translate("MainWindow", "Startup", nullptr));
        pbSend->setText(QApplication::translate("MainWindow", "Send", nullptr));
        pushButton_7->setText(QApplication::translate("MainWindow", "Emergency Stop", nullptr));
        gbx_Test_Parameters->setTitle(QApplication::translate("MainWindow", "Test Parameters", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Feedrate", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Extrusion", nullptr));
        lnExtrusion->setText(QApplication::translate("MainWindow", "1.5", nullptr));
        lnFeedrate->setText(QApplication::translate("MainWindow", "200", nullptr));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", nullptr));
        menuConnections->setTitle(QApplication::translate("MainWindow", "Connections", nullptr));
        menuBaud_Rate->setTitle(QApplication::translate("MainWindow", "Baud Rate:", nullptr));
        menuPSO_Portal->setTitle(QApplication::translate("MainWindow", "PSO Portal", nullptr));
        menuDAQ_Portal->setTitle(QApplication::translate("MainWindow", "DAQ Portal", nullptr));
        menuSettings->setTitle(QApplication::translate("MainWindow", "Settings", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
