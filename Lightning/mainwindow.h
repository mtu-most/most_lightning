#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbConnect_released();
    void UpdateTerminal(QString Message);
    void Fill_Baud_Rates();
    void on_pbSend_released();
    void on_lnCommand_returnPressed();

    void ConnectionSettingsChanged(QString Port, qint32 Baud);
    void on_pushButton_3_released();

    void on_pushButton_7_released();

    void on_pushButton_6_released();

    void on_pushButton_5_released();

    void on_pushButton_4_released();

    void on_pushButton_2_released();

    void on_xDec_released();
    void on_xInc_released();
    void on_yDec_released();
    void on_yInc_released();
    void on_zDec_released();
    void on_zInc_released();


    void on_endEffectToggler_toggled(bool checked);

    void on_incrementSize_valueChanged(double arg1);

    void on_exhaustFan_toggled(bool checked);

    void on_testWeldXInc_released();

    void on_pushButton_released();

    void on_moveToNextXTest_released();
    void on_moveToNextZTest_released();

    void on_measurementRoutine_toggled(bool checked);

    void on_startupPosition_released();

    void on_setZHeight_released();

private:
    Ui::MainWindow *ui;

//protected:
//    void keyPressEvent(QKeyEvent *e) override;

};

#endif // MAINWINDOW_H
