#ifndef GCODEPROCESS_H
#define GCODEPROCESS_H

//Dependancies
#include <QtSerialPort/QSerialPort> //To handle Serial Communication
#include <QTimer> //For periodically occuring events
#include <QObject> //For Object Oriented Things

//Program Constants & Parameters
#define RxFreq 1 //How often to check for new received messages (in ms)
#define TxFreq 1 //How often to attempt to send queued commands (in ms)

class GCodeProcess : public QObject
{
    Q_OBJECT
public:
    explicit GCodeProcess(QObject *parent = nullptr);
    bool Initialize(QString PortName, qint32 BaudRate, QSerialPort::DataBits DataBits = QSerialPort::Data8, QSerialPort::Parity Parity = QSerialPort::NoParity, QSerialPort::StopBits StopBits = QSerialPort::OneStop, QSerialPort::FlowControl FlowControl = QSerialPort::NoFlowControl);
    bool Terminate();
    QString CurrentPort();
    qint32 CurrentBaudRate();
    void Send(QString Command);
    void QueueCommand(QString Command, bool Priority);
    void RunFile(QString Path);
    QString ExtractPrefix(QString Command);
    QString InsertSpaces(QString Command);

private:
    bool CommandFilter(QString Command);

signals:
    void MessageReceived(QString Message);
    void CommandSent(QString Message);

private slots:
    void Receive();
    void ServiceQueue();
};

#endif // GCODEPROCESS_H
