#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "gcodeprocess.h"
#include <unistd.h>

//Public Entities
GCodeProcess GCode;
double incrementSizeInMM = 1.00;
unsigned int second = 1000000;
QList<QString> commandList;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Connect signals to their slots
    QObject::connect(&GCode,SIGNAL(MessageReceived(QString)), this, SLOT(UpdateTerminal(QString)));
    QObject::connect(&GCode,SIGNAL(CommandSent(QString)), this, SLOT(UpdateTerminal(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbConnect_released()
{
    QString Name = "ttyACM0";
    GCode.Initialize(Name,250000);
}

void MainWindow::UpdateTerminal(QString Message)
{
    ui->txtTerminal->append(Message);
}

void MainWindow::on_pbSend_released()
{
    QString Command = ui->lnCommand->text();
    ui->lnCommand->clear();
    GCode.QueueCommand(Command,0);
}

void MainWindow::on_lnCommand_returnPressed()
{
    //This has the same exact effect as pressing the Send button
    QString Command = ui->lnCommand->text();
    ui->lnCommand->clear();
    GCode.QueueCommand(Command,0);
    commandList.append(Command);
}

//void MainWindow::keyPressEvent(QKeyEvent *e)
//{
//    switch (e->key()) {
//    case Qt::Key_Up:
//        ui->lnCommand = commandList.last();
//        break;
//    default:
//        break;
//    }
//}

void MainWindow::Fill_Baud_Rates()
{
    //First Delete all of the children
    ui->menuBaud_Rate->clear();
    //Add the most common baud rates
    QAction *Baud1Action = ui->menuBaud_Rate->addAction("9600");
    QAction *Baud2Action = ui->menuBaud_Rate->addAction("19200");
    QAction *Baud3Action = ui->menuBaud_Rate->addAction("38400");
    QAction *Baud4Action = ui->menuBaud_Rate->addAction("115200");
    QAction *Baud5Action = ui->menuBaud_Rate->addAction("250000");
    //QObject::connect(Action,SIGNAL(triggered()),someObj,SLOT(actionReaction()));
}

void MainWindow::ConnectionSettingsChanged(QString Port, qint32 Baud)
{
    /////////////////////////////////////////////////////////////
    /// This function runs whenever a connection setting changes
    /// if there is a change, the connection is terminated and
    /// a new one is created.
    /////////////////////////////////////////////////////////////

    if (!((Port == GCode.CurrentPort()) & (Baud == GCode.CurrentBaudRate()))) //If something has changed...
    {
        //GCode.
    }
}

void MainWindow::on_pushButton_3_released()
{
    Fill_Baud_Rates();
}

void MainWindow::on_pushButton_7_released()
{
    GCode.Send("M112");
}

void MainWindow::on_pushButton_6_released()
{
    GCode.QueueCommand("G28 X",0);
}

void MainWindow::on_pushButton_5_released()
{
    GCode.QueueCommand("G28 Y",0);
}

void MainWindow::on_pushButton_4_released()
{
    GCode.QueueCommand("G28 Z",0);
}

void MainWindow::on_pushButton_2_released()
{
    //GCode.QueueCommand("M0",0);
    //GCode.Send("");
}

void MainWindow::on_xDec_released()
{
    GCode.QueueCommand("G91",0);
    QString command = "G1 F1000 X-";
    command.append(QString("%1").arg(incrementSizeInMM));
    GCode.QueueCommand(command,0);
}

void MainWindow::on_xInc_released()
{
    GCode.QueueCommand("G91",0);
    QString command = "G1 F1000 X";
    command.append(QString("%1").arg(incrementSizeInMM));
    GCode.QueueCommand(command,0);
}

void MainWindow::on_yDec_released()
{
    GCode.QueueCommand("G91",0);
    QString command = "G1 F1000 Y-";
    command.append(QString("%1").arg(incrementSizeInMM));
    GCode.QueueCommand(command,0);
}

void MainWindow::on_yInc_released()
{
    GCode.QueueCommand("G91",0);
    QString command = "G1 F1000 Y";
    command.append(QString("%1").arg(incrementSizeInMM));
    GCode.QueueCommand(command,0);
}

void MainWindow::on_zDec_released()
{
    GCode.QueueCommand("G91",0);
    QString command = "G1 F250 Z-";
    command.append(QString("%1").arg(incrementSizeInMM));
    GCode.QueueCommand(command,0);
}

void MainWindow::on_zInc_released()
{
    GCode.QueueCommand("G91",0);
    QString command = "G1 F250 Z";
    command.append(QString("%1").arg(incrementSizeInMM));
    GCode.QueueCommand(command,0);
}

void MainWindow::on_endEffectToggler_toggled(bool checked)
{
    if (checked) {
        GCode.QueueCommand("M106 P0",0);
    } else {
        GCode.QueueCommand("M107 P0",0);
    }
}

void MainWindow::on_incrementSize_valueChanged(double arg1)
{
    incrementSizeInMM = arg1;
}

void MainWindow::on_exhaustFan_toggled(bool checked)
{
    if (checked) {
        GCode.QueueCommand("M106 P1",0);
    } else {
        GCode.QueueCommand("M107 P1",0);
    }
}

void MainWindow::on_testWeldXInc_released()
{
    // Get experiment values from the ui
    float feedrate = ui->lnFeedrate->text().toFloat();
    float extrusion = ui->lnExtrusion->text().toFloat();
    float zHeight = ui->lnHeight->text().toFloat();
    float arcLinger = 0.2;

    // Get correct zheight offset from total desired height
    float zHeightDefault = 1.1;
    float zHeightOffset = zHeight - zHeightDefault;

    // Conver seconds to milliseconds
    arcLinger = arcLinger*1000;

    GCode.QueueCommand("G91",0);                            // Set relative movement
    GCode.QueueCommand("M92 E106.01",0);                    // Set the number of steps per mm for the extruder
    GCode.QueueCommand("M203 X2000 Y2000 Z2000 E2000",0);   // Max out feedrates
    GCode.QueueCommand("M106 P1",0);                        // Turn on exhaust fan
    GCode.QueueCommand("G1 F" + QString::number(feedrate/2) + " X4",0);  // Start move
    GCode.QueueCommand("M106 P0",0);                        // Turn on welder
    GCode.QueueCommand("G4 P" + QString::number(arcLinger),0);  // Sleep for a bit
    GCode.QueueCommand("G1 E" + QString::number(extrusion*16) + " F" + QString::number(feedrate) + " X16",0);   // Extrude welding wire 20mm along 20mm move
    GCode.QueueCommand("M107 P0",0);                        // Turn off welder
    GCode.QueueCommand("G1 F" + QString::number(feedrate/2) + " X4",0); // Finish off rest of the move
    GCode.QueueCommand("G92 X0 Y0 Z0",0);                   // Set zero to point of last test

}

void MainWindow::on_pushButton_released()
{

}

void MainWindow::on_moveToNextXTest_released()
{
    float lineYDist = ui->lnYDist->text().toFloat();

    GCode.QueueCommand("G91",0);
    GCode.QueueCommand("G1 F500 Y" + QString::number(lineYDist+2),0);
    GCode.QueueCommand("G1 F2000 X-24",0);
    GCode.QueueCommand("G1 F500 Y-2",0);
    GCode.QueueCommand("G92 X0 Y0 Z0",0);
}

void MainWindow::on_moveToNextZTest_released()
{
    float planeZDist = ui->lnZDist->text().toFloat();
    float lineYDist = ui->lnYDist->text().toFloat();

    GCode.QueueCommand("G91",0);
    GCode.QueueCommand("G1 F250 Z8",0);
    GCode.QueueCommand("G1 F2000 Y-" + QString::number(lineYDist*11),0);
    GCode.QueueCommand("G1 F2000 X-24",0);
    GCode.QueueCommand("G1 F250 Z-" + QString::number(8-planeZDist),0);
    GCode.QueueCommand("G92 X0 Y0 Z0",0);
}

void MainWindow::on_measurementRoutine_toggled(bool checked)
{
    if (checked) {
        GCode.QueueCommand("G92 X0 Y0 Z0",0);
        GCode.QueueCommand("G91",0);
        GCode.QueueCommand("G1 Z2",0);
        GCode.QueueCommand("G1 X50 Y170 F2000",0);
        GCode.QueueCommand("G91",0);
    } else {
        GCode.QueueCommand("G90",0);
        GCode.QueueCommand("G1 X0 Y0 Z20 F2000",0);
        GCode.QueueCommand("G1 Z0 F250",0);
        GCode.QueueCommand("G91",0);
    }
}

void MainWindow::on_startupPosition_released()
{
    GCode.QueueCommand("G91",0);
    GCode.QueueCommand("G1 F250 Z2",0);
    GCode.QueueCommand("G28 X",0);
    GCode.QueueCommand("G28 Y",0);
    GCode.QueueCommand("G28 Z",0);
    GCode.QueueCommand("G1 F500 Y32 Z19",0);
    GCode.QueueCommand("G1 F500 X39",0);
    GCode.QueueCommand("G1 F250 Z-8",0);
    GCode.QueueCommand("G92 X0 Y0 Z0",0);
}

void MainWindow::on_setZHeight_released()
{
    float zHeight = ui->lnHeight->text().toFloat();

    // Get correct zheight offset from total desired height
    float zHeightDefault = 1.1;
    float zHeightOffset = zHeight - zHeightDefault;

    GCode.QueueCommand("G1 F100 Z" + QString::number(zHeightOffset),0);
}
