//This .cpp file is responsible for both sending and receiving G-Code Commands via Serial.

//Headers
#include "gcodeprocess.h"
#include "gcodedefinitions.h" //Defines Language Specific G-Code terms

//Dependancies
#include <QString> //To utilize all the features for the QString Datatype
#include <QtSerialPort/QSerialPort> //To handle Serial Communication
#include <QDebug> //To view Debug Messages in QtCreator Terminal
#include <QTimer> //For periodically occuring events
#include <QFile> //For Handling text files
#include <QTextStream> //To easily handle file as a stream of text



//Public Entities
QSerialPort Serial;
bool MachineReady = true;
QTimer *ReceiveTimer;
QTimer *QueueTimer;
QStringList GCodeQueue;
int GCodeQueueCurrentLine=0;

GCodeProcess::GCodeProcess(QObject *parent) : QObject(parent)
{
    ///////////////////////////////////////////////////////////////
    /// This is the constructor function
    /// It runs whenever a new GCodeProcess is instantiated
    ///////////////////////////////////////////////////////////////

    //Make connections for send and receive timer
    ReceiveTimer = new QTimer(this);
    QObject::connect(ReceiveTimer, SIGNAL(timeout()), this, SLOT(Receive()));

    QueueTimer = new QTimer(this);
    QObject::connect(QueueTimer, SIGNAL(timeout()), this, SLOT(ServiceQueue()));

}

bool GCodeProcess::Initialize(QString PortName, qint32 BaudRate, QSerialPort::DataBits DataBits, QSerialPort::Parity Parity, QSerialPort::StopBits StopBits, QSerialPort::FlowControl FlowControl)
{
    ///////////////////////////////////////////////////////////////
    /// This function begins the serial communication with the device
    /// And also begins the Receive Handler
    ///////////////////////////////////////////////////////////////

    if(Serial.isOpen())
    {
        qDebug() << "A serial is already open";
        return 0; //Indicate that the serial port was not connected to
    }
    else
    {
        //Define Port Properties
        Serial.setPortName(PortName);
        Serial.setBaudRate(BaudRate);
        Serial.setDataBits(DataBits);
        Serial.setParity(Parity);
        Serial.setStopBits(StopBits);
        Serial.setFlowControl(FlowControl);

        //Now Attempt to Connect to the Port
        if(Serial.open(QIODevice::ReadWrite))
        {
            qDebug() << "Serial Connection Successful";

            //Immediatly start receiving data from machine
            ReceiveTimer->start(RxFreq);


            return 1; //Indicate that the serial port was connected to
        }
        else
        {
            qDebug() << "Serial Connection Failed";
            return 0; //Indicate that the serial port was not connected to
        }
    }
}

bool GCodeProcess::Terminate()
{
    //////////////////////////////////////////////////
    /// This function ends the current serial process
    /// and returns a 1 on success
    //////////////////////////////////////////////////

    ReceiveTimer->stop(); // Stop the Receive Timer
    QueueTimer->stop(); //Stop the Send Timer
    Serial.close(); //Close the Serial
    return true;
}
QString GCodeProcess::CurrentPort()
{
    //Return the current Serial Port Name
    return Serial.portName();
}
qint32 GCodeProcess::CurrentBaudRate()
{
    //Return the current baud rate
    return Serial.baudRate();
}

void GCodeProcess::Send(QString Command)
{
    ///////////////////////////////////////////////////////////////
    /// This function is responsible for sending GCode Commands
    /// It assumes that it is always okay to send a command
    /// This command should not be used directly - instead use QueueGCode()
    ///////////////////////////////////////////////////////////////

    QByteArray CommandBytes = Command.toLocal8Bit(); //Convert the string to a usable format
    if(Serial.isOpen()){
        Serial.write(CommandBytes + "\n"); //Send command & append new line characer
        //Indicate that a command has been sent
        qDebug() << "Tx: " << Command;
        emit CommandSent("Tx: " + Command);
        MachineReady = false; //Assert that since a command is being sent, the machine is not ready till it acknowledges
    }
    else
    {
        qDebug() << "Cannot send command since serial port is closed";
    }
}

void GCodeProcess::QueueCommand(QString Command, bool Priority = 0)
{
    ///////////////////////////////////////////////////////////////
    /// Runs a command through the filter, and if it's valid, add it to the queue
    /// If the queue isn't being serviced, it will now be serviced
    ///////////////////////////////////////////////////////////////

    //Add spaces in the command where nessisary - if nessisary
    Command = InsertSpaces(Command);

    if (CommandFilter(Command))//This checks if it is a special command - and if it should be sent
    {
        if (Priority)
            GCodeQueue.prepend(Command);//Add Command to start of the Queue
        else
            GCodeQueue.append(Command);//Add Command to end of the Queue
        if (!(QueueTimer->isActive()))
            QueueTimer->start(TxFreq);//Start Servicing the queue if not all ready
    }
}

void GCodeProcess::ServiceQueue()
{
    ///////////////////////////////////////////////////////////////
    /// This function is a slot which is periodically called by a timer
    /// If the machine is ready, it will send the next GCode Command in queue
    ///////////////////////////////////////////////////////////////

    //If the machine is ready, send the command
    if (MachineReady)
    {
        //Send top GCode command, and then clear it.
        Send(GCodeQueue.takeFirst());
    }
    else
    {
        return;
    }

    //If the Queue is empty - no reason to keep running the timer.
    if (GCodeQueue.isEmpty())
    {
        QueueTimer->stop();
        return;
    }
}

void GCodeProcess::Receive()
{
    ///////////////////////////////////////////////////////////////
    /// This function is a slot which is periodically called by a timer
    /// It reads & indicates the received message
    ///////////////////////////////////////////////////////////////

    QString data=""; // Create a string to receive the incoming message
    if(Serial.canReadLine()) //See if there is serial data available on the port
    {
        data = Serial.readLine(); //Read the data
        data.remove("\n"); //Remove new line character for formatting reasons
        if (data!="") //Make Sure the data isn't blank
        {
            //Report the Received Data
            qDebug() << "Rx: " << data;
            emit MessageReceived("Rx: " + data);

            //Check and see if this is an acknowledgement statement - if indicate the machine is ready
            QString GCodeAck = GCodeAcknowledge;
            if (data.contains(GCodeAck)){
                MachineReady=true;
                return;
            }
        }
    }
}

void GCodeProcess::RunFile(QString Path)
{
    ///////////////////////////////////////////////////////////////////////////
    /// This function loads a G-Code multi-line file into the queue and runs it
    ///////////////////////////////////////////////////////////////////////////

    QFile File(Path);
    File.open(QIODevice::ReadOnly | QIODevice::Text); //Open the file
    QTextStream textStream(&File); //convert it to a test stream
    while(true) //Run this until you've reached past the last line
    {
        QString line = textStream.readLine();//Read in the next line in the stream
        if (line.isNull()) //If line is null - we are done
            break;
        else{
            QueueCommand(line);
           }
    }
}

bool GCodeProcess::CommandFilter(QString Command)
{
    ///////////////////////////////////////////////////////////////////////////
    /// This function is made to detect commands to potentially substitute
    /// or cancel certain functions. Additionally it may be used to track
    /// certain parameters based off commands
    ///////////////////////////////////////////////////////////////////////////

    if (Command == "")
    {
        //An empty line - don't send
        return false;
    }
    else return true;
}

QString GCodeProcess::InsertSpaces(QString Command)
{
    ///////////////////////////////////////////////////////////////////////////
    /// Sometimes commands don't have spaces between their parts
    /// This function makes sure they're there
    ///////////////////////////////////////////////////////////////////////////

    if (!Command.contains(" X"))
        Command.replace("X"," X");
    if (!Command.contains(" Y"))
        Command.replace("Y"," Y");
    if (!Command.contains(" Z"))
        Command.replace("Z"," Z");
    if (!Command.contains(" F"))
        Command.replace("F"," F");
    if (!Command.contains(" E"))
        Command.replace("E"," E");
    return Command;
}

QString GCodeProcess::ExtractPrefix(QString Command)
{
        QRegExp rx("(\\ |\\X|\\Y|\\Z|\\E|\\F)"); //RegEx for ' ' or 'X' or 'Y' or 'Z' or 'E' or 'F'
        QStringList CommandParts = Command.split(rx);
        return CommandParts[0];
}
