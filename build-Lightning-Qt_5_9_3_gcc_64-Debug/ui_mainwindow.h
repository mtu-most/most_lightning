/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSelect_a_rate;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QPushButton *pbSend;
    QPushButton *pushButton_7;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QComboBox *comboBox_2;
    QComboBox *comboBox;
    QPushButton *pbConnect;
    QTextEdit *txtTerminal;
    QLineEdit *lnCommand;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_5;
    QCheckBox *exhaustFan;
    QCheckBox *measurementRoutine;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QCheckBox *endEffectToggler;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_6;
    QPushButton *pushButton_5;
    QPushButton *pushButton_4;
    QGroupBox *groupBox_2;
    QLabel *label;
    QDoubleSpinBox *incrementSize;
    QPushButton *testWeldXInc;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_3;
    QPushButton *zDec;
    QPushButton *xDec;
    QPushButton *yDec;
    QPushButton *xInc;
    QPushButton *yInc;
    QPushButton *zInc;
    QPushButton *moveToNextTest;
    QPushButton *startupPosition;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuConnections;
    QMenu *menuBaud_Rate;
    QMenu *menuPSO_Portal;
    QMenu *menuDAQ_Portal;
    QMenu *menuSettings;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(763, 547);
        actionSelect_a_rate = new QAction(MainWindow);
        actionSelect_a_rate->setObjectName(QStringLiteral("actionSelect_a_rate"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        pbSend = new QPushButton(centralWidget);
        pbSend->setObjectName(QStringLiteral("pbSend"));

        gridLayout->addWidget(pbSend, 10, 2, 1, 1);

        pushButton_7 = new QPushButton(centralWidget);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(204, 0, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(255, 51, 51, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(229, 25, 25, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(102, 0, 0, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(136, 0, 0, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush6(QColor(255, 255, 255, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        QBrush brush7(QColor(229, 127, 127, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush7);
        QBrush brush8(QColor(255, 255, 220, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        QBrush brush9(QColor(0, 0, 0, 128));
        brush9.setStyle(Qt::NoBrush);
        palette.setBrush(QPalette::Active, QPalette::PlaceholderText, brush9);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        QBrush brush10(QColor(0, 0, 0, 128));
        brush10.setStyle(Qt::NoBrush);
        palette.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush10);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        QBrush brush11(QColor(0, 0, 0, 128));
        brush11.setStyle(Qt::NoBrush);
        palette.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush11);
        pushButton_7->setPalette(palette);
        pushButton_7->setFlat(false);

        gridLayout->addWidget(pushButton_7, 10, 3, 1, 1);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        comboBox_2 = new QComboBox(groupBox);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));

        gridLayout_2->addWidget(comboBox_2, 1, 0, 1, 1);

        comboBox = new QComboBox(groupBox);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        gridLayout_2->addWidget(comboBox, 1, 1, 1, 1);

        pbConnect = new QPushButton(groupBox);
        pbConnect->setObjectName(QStringLiteral("pbConnect"));

        gridLayout_2->addWidget(pbConnect, 0, 0, 1, 2);


        gridLayout->addWidget(groupBox, 0, 3, 2, 1);

        txtTerminal = new QTextEdit(centralWidget);
        txtTerminal->setObjectName(QStringLiteral("txtTerminal"));

        gridLayout->addWidget(txtTerminal, 0, 1, 9, 2);

        lnCommand = new QLineEdit(centralWidget);
        lnCommand->setObjectName(QStringLiteral("lnCommand"));

        gridLayout->addWidget(lnCommand, 10, 1, 1, 1);

        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        verticalLayout_2 = new QVBoxLayout(groupBox_3);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        exhaustFan = new QCheckBox(groupBox_3);
        exhaustFan->setObjectName(QStringLiteral("exhaustFan"));

        horizontalLayout_5->addWidget(exhaustFan);

        measurementRoutine = new QCheckBox(groupBox_3);
        measurementRoutine->setObjectName(QStringLiteral("measurementRoutine"));

        horizontalLayout_5->addWidget(measurementRoutine);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        pushButton = new QPushButton(groupBox_3);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout_2->addWidget(pushButton);

        pushButton_2 = new QPushButton(groupBox_3);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        horizontalLayout_2->addWidget(pushButton_2);

        endEffectToggler = new QCheckBox(groupBox_3);
        endEffectToggler->setObjectName(QStringLiteral("endEffectToggler"));

        horizontalLayout_2->addWidget(endEffectToggler);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pushButton_6 = new QPushButton(groupBox_3);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));

        horizontalLayout->addWidget(pushButton_6);

        pushButton_5 = new QPushButton(groupBox_3);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));

        horizontalLayout->addWidget(pushButton_5);

        pushButton_4 = new QPushButton(groupBox_3);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        horizontalLayout->addWidget(pushButton_4);


        verticalLayout_2->addLayout(horizontalLayout);


        gridLayout->addWidget(groupBox_3, 7, 3, 2, 1);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        label = new QLabel(groupBox_2);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(80, 20, 141, 20));
        incrementSize = new QDoubleSpinBox(groupBox_2);
        incrementSize->setObjectName(QStringLiteral("incrementSize"));
        incrementSize->setGeometry(QRect(230, 20, 71, 26));
        incrementSize->setValue(1);
        testWeldXInc = new QPushButton(groupBox_2);
        testWeldXInc->setObjectName(QStringLiteral("testWeldXInc"));
        testWeldXInc->setGeometry(QRect(10, 50, 121, 28));
        layoutWidget = new QWidget(groupBox_2);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(70, 80, 171, 131));
        gridLayout_3 = new QGridLayout(layoutWidget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setSizeConstraint(QLayout::SetMaximumSize);
        gridLayout_3->setHorizontalSpacing(5);
        gridLayout_3->setVerticalSpacing(3);
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        zDec = new QPushButton(layoutWidget);
        zDec->setObjectName(QStringLiteral("zDec"));

        gridLayout_3->addWidget(zDec, 2, 3, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        xDec = new QPushButton(layoutWidget);
        xDec->setObjectName(QStringLiteral("xDec"));
        xDec->setMaximumSize(QSize(85, 28));

        gridLayout_3->addWidget(xDec, 1, 0, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        yDec = new QPushButton(layoutWidget);
        yDec->setObjectName(QStringLiteral("yDec"));

        gridLayout_3->addWidget(yDec, 2, 1, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        xInc = new QPushButton(layoutWidget);
        xInc->setObjectName(QStringLiteral("xInc"));

        gridLayout_3->addWidget(xInc, 1, 2, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        yInc = new QPushButton(layoutWidget);
        yInc->setObjectName(QStringLiteral("yInc"));

        gridLayout_3->addWidget(yInc, 0, 1, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        zInc = new QPushButton(layoutWidget);
        zInc->setObjectName(QStringLiteral("zInc"));

        gridLayout_3->addWidget(zInc, 0, 3, 1, 1);

        moveToNextTest = new QPushButton(groupBox_2);
        moveToNextTest->setObjectName(QStringLiteral("moveToNextTest"));
        moveToNextTest->setGeometry(QRect(140, 50, 121, 28));
        startupPosition = new QPushButton(groupBox_2);
        startupPosition->setObjectName(QStringLiteral("startupPosition"));
        startupPosition->setGeometry(QRect(0, 180, 61, 28));

        gridLayout->addWidget(groupBox_2, 2, 3, 5, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 763, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuConnections = new QMenu(menuBar);
        menuConnections->setObjectName(QStringLiteral("menuConnections"));
        menuBaud_Rate = new QMenu(menuConnections);
        menuBaud_Rate->setObjectName(QStringLiteral("menuBaud_Rate"));
        menuPSO_Portal = new QMenu(menuBar);
        menuPSO_Portal->setObjectName(QStringLiteral("menuPSO_Portal"));
        menuDAQ_Portal = new QMenu(menuBar);
        menuDAQ_Portal->setObjectName(QStringLiteral("menuDAQ_Portal"));
        menuSettings = new QMenu(menuBar);
        menuSettings->setObjectName(QStringLiteral("menuSettings"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuConnections->menuAction());
        menuBar->addAction(menuPSO_Portal->menuAction());
        menuBar->addAction(menuDAQ_Portal->menuAction());
        menuBar->addAction(menuSettings->menuAction());
        menuConnections->addAction(menuBaud_Rate->menuAction());
        menuConnections->addSeparator();
        menuBaud_Rate->addAction(actionSelect_a_rate);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        actionSelect_a_rate->setText(QApplication::translate("MainWindow", "Select a rate", Q_NULLPTR));
        pbSend->setText(QApplication::translate("MainWindow", "Send", Q_NULLPTR));
        pushButton_7->setText(QApplication::translate("MainWindow", "Emergency Stop", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MainWindow", "Connection", Q_NULLPTR));
        pbConnect->setText(QApplication::translate("MainWindow", "Connect", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Functions", Q_NULLPTR));
        exhaustFan->setText(QApplication::translate("MainWindow", "Exaust Fan", Q_NULLPTR));
        measurementRoutine->setText(QApplication::translate("MainWindow", "Measurement Routine", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MainWindow", "Run G-Code", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MainWindow", "Pause", Q_NULLPTR));
        endEffectToggler->setText(QApplication::translate("MainWindow", "End Effect", Q_NULLPTR));
        pushButton_6->setText(QApplication::translate("MainWindow", "Home X", Q_NULLPTR));
        pushButton_5->setText(QApplication::translate("MainWindow", "Home Y", Q_NULLPTR));
        pushButton_4->setText(QApplication::translate("MainWindow", "Home Z", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Positioning", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Increment Size (in mm)", Q_NULLPTR));
        testWeldXInc->setText(QApplication::translate("MainWindow", "Test Weld in X+", Q_NULLPTR));
        zDec->setText(QApplication::translate("MainWindow", "Z-", Q_NULLPTR));
        xDec->setText(QApplication::translate("MainWindow", "X-", Q_NULLPTR));
        yDec->setText(QApplication::translate("MainWindow", "Y-", Q_NULLPTR));
        xInc->setText(QApplication::translate("MainWindow", "X+", Q_NULLPTR));
        yInc->setText(QApplication::translate("MainWindow", "Y+", Q_NULLPTR));
        zInc->setText(QApplication::translate("MainWindow", "Z+", Q_NULLPTR));
        moveToNextTest->setText(QApplication::translate("MainWindow", "Move to next test", Q_NULLPTR));
        startupPosition->setText(QApplication::translate("MainWindow", "Startup", Q_NULLPTR));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", Q_NULLPTR));
        menuConnections->setTitle(QApplication::translate("MainWindow", "Connections", Q_NULLPTR));
        menuBaud_Rate->setTitle(QApplication::translate("MainWindow", "Baud Rate:", Q_NULLPTR));
        menuPSO_Portal->setTitle(QApplication::translate("MainWindow", "PSO Portal", Q_NULLPTR));
        menuDAQ_Portal->setTitle(QApplication::translate("MainWindow", "DAQ Portal", Q_NULLPTR));
        menuSettings->setTitle(QApplication::translate("MainWindow", "Settings", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
