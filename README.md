# Lightning

Control software for RepRap metal 3D printing.
Uses QT-5.12.8.

## Installation
```bash
sudo apt update
sudo apt install -y qtcreator qtbase5-examples qtbase5-doc-html
sudo apt install -y qt5-doc qt5-doc-html qt5serialport-examples
git clone most_lightning
```

## Use

